positive_count = 0
even_sum = 0

for _ in range(int(input("N = "))):
    if not (c := int(input())) & 2:
        even_sum += c
    if c > 0:
        positive_count += 1

print(f'Количество положительных чисел: {positive_count}')
print(f'Сумма чётных чисел: {even_sum}')
