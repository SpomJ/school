def progression():
    cur = 1
    while 1:
        yield cur
        cur *= -1/2

generator = progression()
print(sum((next(generator) for _ in range(int(input())))))
